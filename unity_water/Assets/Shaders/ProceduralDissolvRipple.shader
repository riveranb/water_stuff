// Reference: https://www.shadertoy.com/view/NdyyRG

Shader "Unlit/ProcedrualDissolveRipple"
{
    Properties
    {
        _NoiseTex ("Noise", 2D) = "white" {}
        _Brightness ("Brightness", Float) = 0.5
        _RippleCenter ("Ripple Center", Vector) = (0.5, 0.5, 0, 0)
        _RippleParams ("Ripple Parameters (Speed, Waves, Dissolver)", Vector) = (10, 50, 1, 0)
    }
    SubShader
    {
        Tags
        {
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        LOD 100
        Blend SrcAlpha One

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
                float4 objPos : TEXCOORD1;
            };

            sampler2D _NoiseTex;
            float4 _NoiseTex_ST;
            fixed2 _RippleCenter;
            float4 _RippleParams;
            fixed _Brightness;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _NoiseTex);
                o.objPos = v.vertex;
                return o;
            }

            float SDF_Rectangle(float2 p, float hr)
            {
                float2 delta = abs(p) - hr;
                return length(max(delta, 0)) + min(max(delta.x, delta.y), 0);
            }

            fixed4 frag (v2f i) : SV_Target
            {
                float2 uv = i.objPos.xy - _RippleCenter;
                float leng = length(uv);
                float2 pulse = sin(_Time.y * _RippleParams.x - leng * _RippleParams.y) * normalize(uv);
                pulse = max(pulse, 0) * leng;
                float output = length(pulse) / leng * _Brightness;
                fixed noise = tex2D(_NoiseTex, i.uv);
                float mask = SDF_Rectangle(i.uv - float2(0.5, 0.5), float2(0.4, 0.4));
                float fade = smoothstep(0.1, 0.02, mask);
                output = saturate(output - noise * leng * _RippleParams.z);
                return fixed4(output, output, output, fade);
            }
            ENDCG
        }
    }
}
