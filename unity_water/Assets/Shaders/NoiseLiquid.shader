/// reference: https://shorturl.at/lmnKR

Shader "Unlit/NoiseLiquid"
{
    Properties
    {
        _NoiseTex ("Noise", 2D) = "white" {}
        _MajorFlow ("Major Flow Direction", Vector) = (1, 1, 0.5, 0)
        _SecondFlow ("Second Flow Direction", Vector) = (-1, -1, 0.5, 0)
        _RampLookup ("Ramp Lookup Table", Vector) = (0.2, 0.4, 0.6, 1)
        _GlowColor ("Glow Color", Color) = (1, 1, 1, 1)
        _SpecularColor ("Specular Color", Color) = (0.8, 0.8, 0.8, 1)
        _DiffuseColor ("Diffuse Color", Color) = (0.5, 0.5, 0.5, 1)
        _AmbientColor ("Ambient Color", Color) = (0.1, 0.1, 0.1, 1)
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            sampler2D _NoiseTex;
            float4 _NoiseTex_ST;
            float4 _MajorFlow;
            float4 _SecondFlow;
            float4 _RampLookup;
            float4 _GlowColor;
            float4 _SpecularColor;
            float4 _DiffuseColor;
            float4 _AmbientColor;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _NoiseTex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                float2 majorUV = i.uv + _Time.x * _MajorFlow.xy;
                float2 secondUV = i.uv + _Time.x * _SecondFlow.xy;
                // sample the noise bias
                fixed4 offset = tex2D(_NoiseTex, majorUV) * _MajorFlow.z;
                offset += tex2D(_NoiseTex, secondUV) * _SecondFlow.z;
                float weight = max(1 - _MajorFlow.z - _SecondFlow.z, 0) + 0.5;
                offset += tex2D(_NoiseTex, i.uv) * weight;

                // sample biased color
                fixed4 col = tex2D(_NoiseTex, i.uv + offset.xy) * offset;

                // compute ramped color
                float4x4 colorMatrix = {
                    _AmbientColor,
                    _DiffuseColor,
                    _SpecularColor,
                    _GlowColor
                };
                float4 rampWeight = float4(0, 0, 0, 0);
                rampWeight.x = step(col.r, _RampLookup.x + 0.025f);
                rampWeight.y = smoothstep(_RampLookup.x, _RampLookup.y, col.r) * 
                    smoothstep(_RampLookup.y + 0.05f, _RampLookup.y - 0.05f, col.r);
                rampWeight.z = smoothstep(_RampLookup.y, _RampLookup.z, col.r) * 
                    smoothstep(_RampLookup.z + 0.05f, _RampLookup.z - 0.05f, col.r);
                rampWeight.w = smoothstep(_RampLookup.z - 0.05f, _RampLookup.z + 0.05f, col.r);
                col = mul(rampWeight, colorMatrix);

                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);
                return col;
            }
            ENDCG
        }
    }
}
