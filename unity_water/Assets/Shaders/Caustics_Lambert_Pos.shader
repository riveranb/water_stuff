Shader "Unlit/Caustics_Lambert_Pos"
{
    Properties
    {
        _CausticsTex ("Caustics Pattern", 2D) = "" {}
        _Albedo ("Albedo", Color) = (1, 1, 1, 1)
        _CausticsParams ("Caustics Parameters (Speed2 Tiling2)", Vector) = (1, 1, 1, 1)
        _CausticsSplit ("Chromatic Abberation Caustics Splitter", Float) = 0.5
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #include "AutoLight.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
                float3 normal : TEXCOORD0;
                float4 litcoord : TEXCOORD1;
            };

            float4x4 _WorldToLit_M;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.normal = UnityObjectToWorldNormal(v.normal);
                float4 worldcoord = mul(UNITY_MATRIX_M, v.vertex);
                o.litcoord = mul(_WorldToLit_M, worldcoord);
                return o;
            }

            fixed4 _Albedo;
            half4 _CausticsParams;
            half _CausticsSplit;
            sampler2D _CausticsTex;

            half2 PanUV(half2 uv, half speed, half tiling)
            {
                return (half2(1, 0) * _Time.y * speed) + (uv * tiling);
            }

            fixed3 ChromaticCaustics(sampler2D caustics, half2 uv, half split)
            {
                half2 uv1 = uv + half2(split, split);
                half2 uv2 = uv + half2(split, -split);
                half2 uv3 = uv + half2(-split, -split);

                fixed r = tex2D(caustics, uv1).r;
                fixed g = tex2D(caustics, uv2).r;
                fixed b = tex2D(caustics, uv3).r;

                return fixed3(r, g, b);
            }

            fixed4 frag (v2f i) : SV_Target
            {
                float3 normdir = normalize(i.normal);
                float3 lightdir = normalize(_WorldSpaceLightPos0.xyz);
                fixed3 attenuate_litcol = _LightColor0.rgb * LIGHT_ATTENUATION(i);
                float n_d_l = max(0, dot(normdir, lightdir));
                float diffusecol = attenuate_litcol * n_d_l;
                float env_ambientcol = UNITY_LIGHTMODEL_AMBIENT.rgb;

                // sample the texture
                fixed4 col = _Albedo * (diffusecol + env_ambientcol);
                half2 moving_uv1 = PanUV(i.litcoord.xy, _CausticsParams.x, _CausticsParams.y);
                half2 moving_uv2 = PanUV(i.litcoord.xy, _CausticsParams.z, _CausticsParams.w);
                fixed3 caustics1 = ChromaticCaustics(_CausticsTex, moving_uv1, _CausticsSplit) * (0.1 + n_d_l);
                fixed3 caustics2 = ChromaticCaustics(_CausticsTex, moving_uv2, _CausticsSplit) * (0.1 + n_d_l);
                col.rgb += min(caustics1, caustics2);
                return col;
            }
            ENDCG
        }
    }
}
