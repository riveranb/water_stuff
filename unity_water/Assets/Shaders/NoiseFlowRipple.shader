/// reference: https://shorturl.at/lmnKR

Shader "Unlit/NoiseFlowRipple"
{
    Properties
    {
        _NoiseTex ("Noise", 2D) = "white" {}
        _MajorFlow ("Major Flow", Vector) = (1, 1, 0.5, 0)
        _SecondFlow ("Second Flow", Vector) = (-1, -1, 0.5, 0)
        _DarkWaterColor ("Dark Water Color", Color) = (1, 1, 1, 1)
    }
    SubShader
    {
        Tags
        {
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        LOD 100
        Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _NoiseTex;
            float4 _NoiseTex_ST;
            float4 _MajorFlow;
            float4 _SecondFlow;
            fixed3 _DarkWaterColor;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _NoiseTex);
                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                float2 majorUV = i.uv + _Time.x * _MajorFlow.xy;
                float2 secondUV = i.uv + _Time.x * _SecondFlow.xy;
                // sample the noise bias
                half4 offset1 = tex2D(_NoiseTex, majorUV) * _MajorFlow.z;
                half4 offset2 = tex2D(_NoiseTex, secondUV) * _SecondFlow.z;
                half2 offset = offset1.xy + offset2.xy;

                // sample noise
                fixed4 noise = tex2D(_NoiseTex, i.uv + offset.xy);
                fixed alpha = i.uv.y + noise.r;
                
                fixed dense = smoothstep(1, 0.6, alpha);
                fixed3 color = lerp(_DarkWaterColor, fixed3(1, 1, 1), dense);

                fixed fade = smoothstep(0, 0.1, i.uv.x * i.uv.y);
                fade *= smoothstep(0, 0.1, (1 - i.uv.x) * i.uv.y);
                alpha = (1 - alpha + noise.r * 0.5) * fade;

                return fixed4(color, saturate(alpha));
            }
            ENDCG
        }
    }
}
