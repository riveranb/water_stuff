using UnityEngine;

public class MakeLitMatrix : MonoBehaviour
{
    private int shaderId_WorldToLit_M = Shader.PropertyToID("_WorldToLit_M");
    private Matrix4x4 worldToObj = Matrix4x4.identity;

    private void Update()
    {
        worldToObj = transform.worldToLocalMatrix;
        Shader.SetGlobalMatrix(shaderId_WorldToLit_M, worldToObj);
    }
}
