# water_stuff

My learning lessons - water/liquid visual effect (shaders)

## Water Fluid Ripple

* Stylized liquid (noise texture)

![Stlyized liquid](https://gitlab.com/riveranb/water_stuff/-/raw/main/demo_snapshots/20230704_noised_stylized_liquid_pocV0.gif?ref_type=heads)

* Liquid flow (noise texture)

![Liquid flow](https://gitlab.com/riveranb/water_stuff/-/raw/main/demo_snapshots/20230709_noiseflow_ripple_pocV0.gif?ref_type=heads)

* Procedural (sine wave) ripple

![Sine ripple](https://gitlab.com/riveranb/water_stuff/-/raw/main/demo_snapshots/20230711_procedural_ripple_pocV0.gif?ref_type=heads)

## Caustics

* Chromatic caustics (texture)

![Chromatic caustics](https://gitlab.com/riveranb/water_stuff/-/raw/main/demo_snapshots/20230616_caustics_texture_pocV0.gif?ref_type=heads)
